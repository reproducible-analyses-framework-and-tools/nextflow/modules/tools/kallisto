#!/usr/bin/env nextflow

process kallisto_index {
// Runs kallisto index
//
// input:
//   path fa - Reference FASTA
//   val parstr - Additional Parameters
//   val out_dir - Output Directory
//   val shared_dir - Shared Output Directory
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}*index") - Index Files
//   path('kallisto-*') - For publishDir

  label 'kallisto'
  storeDir "${params.shared_dir}/${fa}/kallisto_index"
  tag "${fa}"
  cache 'lenient'

  input:
  path fa
  val parstr

  output:
  tuple path(fa), path("${fa}*index"), emit: idx_files

  script:
  """
  kallisto index -i ${fa}.index ${parstr} ${fa}
  """
}


process kallisto_quant {
// Runs kallisto quant
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(prefix) - FASTQ Prefix
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   val parstr - Additional Parameters
//   val out_dir - Output Directory
//   val shared_dir - Shared Output Directory
//
// output:
//   tuple => emit: quants
//     val(pat_name)
//     val(prefix)
//     val(dataset)
//     path('*abundance.tsv')
//   tuple => emit: h5_quants
//     val(pat_name)
//     val(prefix)
//     val(dataset)
//     path('*abundance.h5')
//   path('kallisto-*') - For publishDir

  label 'kallisto'
  publishDir "${params.shared_dir}/${dataset}/${pat_name}/${prefix}"
  tag "${dataset}/${pat_name}/${prefix}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(prefix), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(prefix), val(dataset), path('*abundance.tsv'), emit: quants
  tuple val(pat_name), val(prefix), val(dataset), path('*abundance.h5'), emit: h5_quants

  script:
  """
  kallisto quant ${parstr} -t ${task.cpus} -i ${fa}.index -o . ${fq1} ${fq2}
  """
}
